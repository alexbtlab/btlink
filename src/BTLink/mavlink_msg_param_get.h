#pragma once
// MESSAGE PARAM_GET PACKING

#define MAVLINK_MSG_ID_PARAM_GET 8

MAVPACKED(
typedef struct __mavlink_param_get_t {
 uint8_t target; /*<  Идентификатор целевой системы.*/
 uint8_t param; /*<  Идентификатор параметра.*/
}) mavlink_param_get_t;

#define MAVLINK_MSG_ID_PARAM_GET_LEN 2
#define MAVLINK_MSG_ID_PARAM_GET_MIN_LEN 2
#define MAVLINK_MSG_ID_8_LEN 2
#define MAVLINK_MSG_ID_8_MIN_LEN 2

#define MAVLINK_MSG_ID_PARAM_GET_CRC 65
#define MAVLINK_MSG_ID_8_CRC 65



#if MAVLINK_COMMAND_24BIT
#define MAVLINK_MESSAGE_INFO_PARAM_GET { \
    8, \
    "PARAM_GET", \
    2, \
    {  { "target", NULL, MAVLINK_TYPE_UINT8_T, 0, 0, offsetof(mavlink_param_get_t, target) }, \
         { "param", NULL, MAVLINK_TYPE_UINT8_T, 0, 1, offsetof(mavlink_param_get_t, param) }, \
         } \
}
#else
#define MAVLINK_MESSAGE_INFO_PARAM_GET { \
    "PARAM_GET", \
    2, \
    {  { "target", NULL, MAVLINK_TYPE_UINT8_T, 0, 0, offsetof(mavlink_param_get_t, target) }, \
         { "param", NULL, MAVLINK_TYPE_UINT8_T, 0, 1, offsetof(mavlink_param_get_t, param) }, \
         } \
}
#endif

/**
 * @brief Pack a param_get message
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 *
 * @param target  Идентификатор целевой системы.
 * @param param  Идентификатор параметра.
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_param_get_pack(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg,
                               uint8_t target, uint8_t param)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_PARAM_GET_LEN];
    _mav_put_uint8_t(buf, 0, target);
    _mav_put_uint8_t(buf, 1, param);

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_PARAM_GET_LEN);
#else
    mavlink_param_get_t packet;
    packet.target = target;
    packet.param = param;

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_PARAM_GET_LEN);
#endif

    msg->msgid = MAVLINK_MSG_ID_PARAM_GET;
    return mavlink_finalize_message(msg, system_id, component_id, MAVLINK_MSG_ID_PARAM_GET_MIN_LEN, MAVLINK_MSG_ID_PARAM_GET_LEN, MAVLINK_MSG_ID_PARAM_GET_CRC);
}

/**
 * @brief Pack a param_get message on a channel
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param target  Идентификатор целевой системы.
 * @param param  Идентификатор параметра.
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_param_get_pack_chan(uint8_t system_id, uint8_t component_id, uint8_t chan,
                               mavlink_message_t* msg,
                                   uint8_t target,uint8_t param)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_PARAM_GET_LEN];
    _mav_put_uint8_t(buf, 0, target);
    _mav_put_uint8_t(buf, 1, param);

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_PARAM_GET_LEN);
#else
    mavlink_param_get_t packet;
    packet.target = target;
    packet.param = param;

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_PARAM_GET_LEN);
#endif

    msg->msgid = MAVLINK_MSG_ID_PARAM_GET;
    return mavlink_finalize_message_chan(msg, system_id, component_id, chan, MAVLINK_MSG_ID_PARAM_GET_MIN_LEN, MAVLINK_MSG_ID_PARAM_GET_LEN, MAVLINK_MSG_ID_PARAM_GET_CRC);
}

/**
 * @brief Encode a param_get struct
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 * @param param_get C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_param_get_encode(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg, const mavlink_param_get_t* param_get)
{
    return mavlink_msg_param_get_pack(system_id, component_id, msg, param_get->target, param_get->param);
}

/**
 * @brief Encode a param_get struct on a channel
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param param_get C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_param_get_encode_chan(uint8_t system_id, uint8_t component_id, uint8_t chan, mavlink_message_t* msg, const mavlink_param_get_t* param_get)
{
    return mavlink_msg_param_get_pack_chan(system_id, component_id, chan, msg, param_get->target, param_get->param);
}

/**
 * @brief Send a param_get message
 * @param chan MAVLink channel to send the message
 *
 * @param target  Идентификатор целевой системы.
 * @param param  Идентификатор параметра.
 */
#ifdef MAVLINK_USE_CONVENIENCE_FUNCTIONS

static inline void mavlink_msg_param_get_send(mavlink_channel_t chan, uint8_t target, uint8_t param)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_PARAM_GET_LEN];
    _mav_put_uint8_t(buf, 0, target);
    _mav_put_uint8_t(buf, 1, param);

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_PARAM_GET, buf, MAVLINK_MSG_ID_PARAM_GET_MIN_LEN, MAVLINK_MSG_ID_PARAM_GET_LEN, MAVLINK_MSG_ID_PARAM_GET_CRC);
#else
    mavlink_param_get_t packet;
    packet.target = target;
    packet.param = param;

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_PARAM_GET, (const char *)&packet, MAVLINK_MSG_ID_PARAM_GET_MIN_LEN, MAVLINK_MSG_ID_PARAM_GET_LEN, MAVLINK_MSG_ID_PARAM_GET_CRC);
#endif
}

/**
 * @brief Send a param_get message
 * @param chan MAVLink channel to send the message
 * @param struct The MAVLink struct to serialize
 */
static inline void mavlink_msg_param_get_send_struct(mavlink_channel_t chan, const mavlink_param_get_t* param_get)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    mavlink_msg_param_get_send(chan, param_get->target, param_get->param);
#else
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_PARAM_GET, (const char *)param_get, MAVLINK_MSG_ID_PARAM_GET_MIN_LEN, MAVLINK_MSG_ID_PARAM_GET_LEN, MAVLINK_MSG_ID_PARAM_GET_CRC);
#endif
}

#if MAVLINK_MSG_ID_PARAM_GET_LEN <= MAVLINK_MAX_PAYLOAD_LEN
/*
  This varient of _send() can be used to save stack space by re-using
  memory from the receive buffer.  The caller provides a
  mavlink_message_t which is the size of a full mavlink message. This
  is usually the receive buffer for the channel, and allows a reply to an
  incoming message with minimum stack space usage.
 */
static inline void mavlink_msg_param_get_send_buf(mavlink_message_t *msgbuf, mavlink_channel_t chan,  uint8_t target, uint8_t param)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char *buf = (char *)msgbuf;
    _mav_put_uint8_t(buf, 0, target);
    _mav_put_uint8_t(buf, 1, param);

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_PARAM_GET, buf, MAVLINK_MSG_ID_PARAM_GET_MIN_LEN, MAVLINK_MSG_ID_PARAM_GET_LEN, MAVLINK_MSG_ID_PARAM_GET_CRC);
#else
    mavlink_param_get_t *packet = (mavlink_param_get_t *)msgbuf;
    packet->target = target;
    packet->param = param;

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_PARAM_GET, (const char *)packet, MAVLINK_MSG_ID_PARAM_GET_MIN_LEN, MAVLINK_MSG_ID_PARAM_GET_LEN, MAVLINK_MSG_ID_PARAM_GET_CRC);
#endif
}
#endif

#endif

// MESSAGE PARAM_GET UNPACKING


/**
 * @brief Get field target from param_get message
 *
 * @return  Идентификатор целевой системы.
 */
static inline uint8_t mavlink_msg_param_get_get_target(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint8_t(msg,  0);
}

/**
 * @brief Get field param from param_get message
 *
 * @return  Идентификатор параметра.
 */
static inline uint8_t mavlink_msg_param_get_get_param(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint8_t(msg,  1);
}

/**
 * @brief Decode a param_get message into a struct
 *
 * @param msg The message to decode
 * @param param_get C-struct to decode the message contents into
 */
static inline void mavlink_msg_param_get_decode(const mavlink_message_t* msg, mavlink_param_get_t* param_get)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    param_get->target = mavlink_msg_param_get_get_target(msg);
    param_get->param = mavlink_msg_param_get_get_param(msg);
#else
        uint8_t len = msg->len < MAVLINK_MSG_ID_PARAM_GET_LEN? msg->len : MAVLINK_MSG_ID_PARAM_GET_LEN;
        memset(param_get, 0, MAVLINK_MSG_ID_PARAM_GET_LEN);
    memcpy(param_get, _MAV_PAYLOAD(msg), len);
#endif
}

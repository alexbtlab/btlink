#pragma once
// MESSAGE PARAM_SET_ACK PACKING

#define MAVLINK_MSG_ID_PARAM_SET_ACK 11

MAVPACKED(
typedef struct __mavlink_param_set_ack_t {
 uint64_t value; /*<  Значение параметра.*/
 uint8_t param; /*<  Идентификатор параметра.*/
 uint8_t type; /*<  Тип параметра.*/
 uint8_t result; /*<  Результат операци.*/
}) mavlink_param_set_ack_t;

#define MAVLINK_MSG_ID_PARAM_SET_ACK_LEN 11
#define MAVLINK_MSG_ID_PARAM_SET_ACK_MIN_LEN 11
#define MAVLINK_MSG_ID_11_LEN 11
#define MAVLINK_MSG_ID_11_MIN_LEN 11

#define MAVLINK_MSG_ID_PARAM_SET_ACK_CRC 48
#define MAVLINK_MSG_ID_11_CRC 48



#if MAVLINK_COMMAND_24BIT
#define MAVLINK_MESSAGE_INFO_PARAM_SET_ACK { \
    11, \
    "PARAM_SET_ACK", \
    4, \
    {  { "param", NULL, MAVLINK_TYPE_UINT8_T, 0, 8, offsetof(mavlink_param_set_ack_t, param) }, \
         { "type", NULL, MAVLINK_TYPE_UINT8_T, 0, 9, offsetof(mavlink_param_set_ack_t, type) }, \
         { "value", NULL, MAVLINK_TYPE_UINT64_T, 0, 0, offsetof(mavlink_param_set_ack_t, value) }, \
         { "result", NULL, MAVLINK_TYPE_UINT8_T, 0, 10, offsetof(mavlink_param_set_ack_t, result) }, \
         } \
}
#else
#define MAVLINK_MESSAGE_INFO_PARAM_SET_ACK { \
    "PARAM_SET_ACK", \
    4, \
    {  { "param", NULL, MAVLINK_TYPE_UINT8_T, 0, 8, offsetof(mavlink_param_set_ack_t, param) }, \
         { "type", NULL, MAVLINK_TYPE_UINT8_T, 0, 9, offsetof(mavlink_param_set_ack_t, type) }, \
         { "value", NULL, MAVLINK_TYPE_UINT64_T, 0, 0, offsetof(mavlink_param_set_ack_t, value) }, \
         { "result", NULL, MAVLINK_TYPE_UINT8_T, 0, 10, offsetof(mavlink_param_set_ack_t, result) }, \
         } \
}
#endif

/**
 * @brief Pack a param_set_ack message
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 *
 * @param param  Идентификатор параметра.
 * @param type  Тип параметра.
 * @param value  Значение параметра.
 * @param result  Результат операци.
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_param_set_ack_pack(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg,
                               uint8_t param, uint8_t type, uint64_t value, uint8_t result)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_PARAM_SET_ACK_LEN];
    _mav_put_uint64_t(buf, 0, value);
    _mav_put_uint8_t(buf, 8, param);
    _mav_put_uint8_t(buf, 9, type);
    _mav_put_uint8_t(buf, 10, result);

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_PARAM_SET_ACK_LEN);
#else
    mavlink_param_set_ack_t packet;
    packet.value = value;
    packet.param = param;
    packet.type = type;
    packet.result = result;

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_PARAM_SET_ACK_LEN);
#endif

    msg->msgid = MAVLINK_MSG_ID_PARAM_SET_ACK;
    return mavlink_finalize_message(msg, system_id, component_id, MAVLINK_MSG_ID_PARAM_SET_ACK_MIN_LEN, MAVLINK_MSG_ID_PARAM_SET_ACK_LEN, MAVLINK_MSG_ID_PARAM_SET_ACK_CRC);
}

/**
 * @brief Pack a param_set_ack message on a channel
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param param  Идентификатор параметра.
 * @param type  Тип параметра.
 * @param value  Значение параметра.
 * @param result  Результат операци.
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_param_set_ack_pack_chan(uint8_t system_id, uint8_t component_id, uint8_t chan,
                               mavlink_message_t* msg,
                                   uint8_t param,uint8_t type,uint64_t value,uint8_t result)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_PARAM_SET_ACK_LEN];
    _mav_put_uint64_t(buf, 0, value);
    _mav_put_uint8_t(buf, 8, param);
    _mav_put_uint8_t(buf, 9, type);
    _mav_put_uint8_t(buf, 10, result);

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_PARAM_SET_ACK_LEN);
#else
    mavlink_param_set_ack_t packet;
    packet.value = value;
    packet.param = param;
    packet.type = type;
    packet.result = result;

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_PARAM_SET_ACK_LEN);
#endif

    msg->msgid = MAVLINK_MSG_ID_PARAM_SET_ACK;
    return mavlink_finalize_message_chan(msg, system_id, component_id, chan, MAVLINK_MSG_ID_PARAM_SET_ACK_MIN_LEN, MAVLINK_MSG_ID_PARAM_SET_ACK_LEN, MAVLINK_MSG_ID_PARAM_SET_ACK_CRC);
}

/**
 * @brief Encode a param_set_ack struct
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 * @param param_set_ack C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_param_set_ack_encode(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg, const mavlink_param_set_ack_t* param_set_ack)
{
    return mavlink_msg_param_set_ack_pack(system_id, component_id, msg, param_set_ack->param, param_set_ack->type, param_set_ack->value, param_set_ack->result);
}

/**
 * @brief Encode a param_set_ack struct on a channel
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param param_set_ack C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_param_set_ack_encode_chan(uint8_t system_id, uint8_t component_id, uint8_t chan, mavlink_message_t* msg, const mavlink_param_set_ack_t* param_set_ack)
{
    return mavlink_msg_param_set_ack_pack_chan(system_id, component_id, chan, msg, param_set_ack->param, param_set_ack->type, param_set_ack->value, param_set_ack->result);
}

/**
 * @brief Send a param_set_ack message
 * @param chan MAVLink channel to send the message
 *
 * @param param  Идентификатор параметра.
 * @param type  Тип параметра.
 * @param value  Значение параметра.
 * @param result  Результат операци.
 */
#ifdef MAVLINK_USE_CONVENIENCE_FUNCTIONS

static inline void mavlink_msg_param_set_ack_send(mavlink_channel_t chan, uint8_t param, uint8_t type, uint64_t value, uint8_t result)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_PARAM_SET_ACK_LEN];
    _mav_put_uint64_t(buf, 0, value);
    _mav_put_uint8_t(buf, 8, param);
    _mav_put_uint8_t(buf, 9, type);
    _mav_put_uint8_t(buf, 10, result);

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_PARAM_SET_ACK, buf, MAVLINK_MSG_ID_PARAM_SET_ACK_MIN_LEN, MAVLINK_MSG_ID_PARAM_SET_ACK_LEN, MAVLINK_MSG_ID_PARAM_SET_ACK_CRC);
#else
    mavlink_param_set_ack_t packet;
    packet.value = value;
    packet.param = param;
    packet.type = type;
    packet.result = result;

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_PARAM_SET_ACK, (const char *)&packet, MAVLINK_MSG_ID_PARAM_SET_ACK_MIN_LEN, MAVLINK_MSG_ID_PARAM_SET_ACK_LEN, MAVLINK_MSG_ID_PARAM_SET_ACK_CRC);
#endif
}

/**
 * @brief Send a param_set_ack message
 * @param chan MAVLink channel to send the message
 * @param struct The MAVLink struct to serialize
 */
static inline void mavlink_msg_param_set_ack_send_struct(mavlink_channel_t chan, const mavlink_param_set_ack_t* param_set_ack)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    mavlink_msg_param_set_ack_send(chan, param_set_ack->param, param_set_ack->type, param_set_ack->value, param_set_ack->result);
#else
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_PARAM_SET_ACK, (const char *)param_set_ack, MAVLINK_MSG_ID_PARAM_SET_ACK_MIN_LEN, MAVLINK_MSG_ID_PARAM_SET_ACK_LEN, MAVLINK_MSG_ID_PARAM_SET_ACK_CRC);
#endif
}

#if MAVLINK_MSG_ID_PARAM_SET_ACK_LEN <= MAVLINK_MAX_PAYLOAD_LEN
/*
  This varient of _send() can be used to save stack space by re-using
  memory from the receive buffer.  The caller provides a
  mavlink_message_t which is the size of a full mavlink message. This
  is usually the receive buffer for the channel, and allows a reply to an
  incoming message with minimum stack space usage.
 */
static inline void mavlink_msg_param_set_ack_send_buf(mavlink_message_t *msgbuf, mavlink_channel_t chan,  uint8_t param, uint8_t type, uint64_t value, uint8_t result)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char *buf = (char *)msgbuf;
    _mav_put_uint64_t(buf, 0, value);
    _mav_put_uint8_t(buf, 8, param);
    _mav_put_uint8_t(buf, 9, type);
    _mav_put_uint8_t(buf, 10, result);

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_PARAM_SET_ACK, buf, MAVLINK_MSG_ID_PARAM_SET_ACK_MIN_LEN, MAVLINK_MSG_ID_PARAM_SET_ACK_LEN, MAVLINK_MSG_ID_PARAM_SET_ACK_CRC);
#else
    mavlink_param_set_ack_t *packet = (mavlink_param_set_ack_t *)msgbuf;
    packet->value = value;
    packet->param = param;
    packet->type = type;
    packet->result = result;

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_PARAM_SET_ACK, (const char *)packet, MAVLINK_MSG_ID_PARAM_SET_ACK_MIN_LEN, MAVLINK_MSG_ID_PARAM_SET_ACK_LEN, MAVLINK_MSG_ID_PARAM_SET_ACK_CRC);
#endif
}
#endif

#endif

// MESSAGE PARAM_SET_ACK UNPACKING


/**
 * @brief Get field param from param_set_ack message
 *
 * @return  Идентификатор параметра.
 */
static inline uint8_t mavlink_msg_param_set_ack_get_param(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint8_t(msg,  8);
}

/**
 * @brief Get field type from param_set_ack message
 *
 * @return  Тип параметра.
 */
static inline uint8_t mavlink_msg_param_set_ack_get_type(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint8_t(msg,  9);
}

/**
 * @brief Get field value from param_set_ack message
 *
 * @return  Значение параметра.
 */
static inline uint64_t mavlink_msg_param_set_ack_get_value(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint64_t(msg,  0);
}

/**
 * @brief Get field result from param_set_ack message
 *
 * @return  Результат операци.
 */
static inline uint8_t mavlink_msg_param_set_ack_get_result(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint8_t(msg,  10);
}

/**
 * @brief Decode a param_set_ack message into a struct
 *
 * @param msg The message to decode
 * @param param_set_ack C-struct to decode the message contents into
 */
static inline void mavlink_msg_param_set_ack_decode(const mavlink_message_t* msg, mavlink_param_set_ack_t* param_set_ack)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    param_set_ack->value = mavlink_msg_param_set_ack_get_value(msg);
    param_set_ack->param = mavlink_msg_param_set_ack_get_param(msg);
    param_set_ack->type = mavlink_msg_param_set_ack_get_type(msg);
    param_set_ack->result = mavlink_msg_param_set_ack_get_result(msg);
#else
        uint8_t len = msg->len < MAVLINK_MSG_ID_PARAM_SET_ACK_LEN? msg->len : MAVLINK_MSG_ID_PARAM_SET_ACK_LEN;
        memset(param_set_ack, 0, MAVLINK_MSG_ID_PARAM_SET_ACK_LEN);
    memcpy(param_set_ack, _MAV_PAYLOAD(msg), len);
#endif
}

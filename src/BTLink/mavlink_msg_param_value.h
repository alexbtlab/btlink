#pragma once
// MESSAGE PARAM_VALUE PACKING

#define MAVLINK_MSG_ID_PARAM_VALUE 10

MAVPACKED(
typedef struct __mavlink_param_value_t {
 uint64_t value; /*<  Значение параметра.*/
 uint8_t param; /*<  Идентификатор параметра.*/
 uint8_t type; /*<  Тип параметра.*/
}) mavlink_param_value_t;

#define MAVLINK_MSG_ID_PARAM_VALUE_LEN 10
#define MAVLINK_MSG_ID_PARAM_VALUE_MIN_LEN 10
#define MAVLINK_MSG_ID_10_LEN 10
#define MAVLINK_MSG_ID_10_MIN_LEN 10

#define MAVLINK_MSG_ID_PARAM_VALUE_CRC 34
#define MAVLINK_MSG_ID_10_CRC 34



#if MAVLINK_COMMAND_24BIT
#define MAVLINK_MESSAGE_INFO_PARAM_VALUE { \
    10, \
    "PARAM_VALUE", \
    3, \
    {  { "param", NULL, MAVLINK_TYPE_UINT8_T, 0, 8, offsetof(mavlink_param_value_t, param) }, \
         { "type", NULL, MAVLINK_TYPE_UINT8_T, 0, 9, offsetof(mavlink_param_value_t, type) }, \
         { "value", NULL, MAVLINK_TYPE_UINT64_T, 0, 0, offsetof(mavlink_param_value_t, value) }, \
         } \
}
#else
#define MAVLINK_MESSAGE_INFO_PARAM_VALUE { \
    "PARAM_VALUE", \
    3, \
    {  { "param", NULL, MAVLINK_TYPE_UINT8_T, 0, 8, offsetof(mavlink_param_value_t, param) }, \
         { "type", NULL, MAVLINK_TYPE_UINT8_T, 0, 9, offsetof(mavlink_param_value_t, type) }, \
         { "value", NULL, MAVLINK_TYPE_UINT64_T, 0, 0, offsetof(mavlink_param_value_t, value) }, \
         } \
}
#endif

/**
 * @brief Pack a param_value message
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 *
 * @param param  Идентификатор параметра.
 * @param type  Тип параметра.
 * @param value  Значение параметра.
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_param_value_pack(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg,
                               uint8_t param, uint8_t type, uint64_t value)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_PARAM_VALUE_LEN];
    _mav_put_uint64_t(buf, 0, value);
    _mav_put_uint8_t(buf, 8, param);
    _mav_put_uint8_t(buf, 9, type);

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_PARAM_VALUE_LEN);
#else
    mavlink_param_value_t packet;
    packet.value = value;
    packet.param = param;
    packet.type = type;

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_PARAM_VALUE_LEN);
#endif

    msg->msgid = MAVLINK_MSG_ID_PARAM_VALUE;
    return mavlink_finalize_message(msg, system_id, component_id, MAVLINK_MSG_ID_PARAM_VALUE_MIN_LEN, MAVLINK_MSG_ID_PARAM_VALUE_LEN, MAVLINK_MSG_ID_PARAM_VALUE_CRC);
}

/**
 * @brief Pack a param_value message on a channel
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param param  Идентификатор параметра.
 * @param type  Тип параметра.
 * @param value  Значение параметра.
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_param_value_pack_chan(uint8_t system_id, uint8_t component_id, uint8_t chan,
                               mavlink_message_t* msg,
                                   uint8_t param,uint8_t type,uint64_t value)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_PARAM_VALUE_LEN];
    _mav_put_uint64_t(buf, 0, value);
    _mav_put_uint8_t(buf, 8, param);
    _mav_put_uint8_t(buf, 9, type);

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_PARAM_VALUE_LEN);
#else
    mavlink_param_value_t packet;
    packet.value = value;
    packet.param = param;
    packet.type = type;

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_PARAM_VALUE_LEN);
#endif

    msg->msgid = MAVLINK_MSG_ID_PARAM_VALUE;
    return mavlink_finalize_message_chan(msg, system_id, component_id, chan, MAVLINK_MSG_ID_PARAM_VALUE_MIN_LEN, MAVLINK_MSG_ID_PARAM_VALUE_LEN, MAVLINK_MSG_ID_PARAM_VALUE_CRC);
}

/**
 * @brief Encode a param_value struct
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 * @param param_value C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_param_value_encode(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg, const mavlink_param_value_t* param_value)
{
    return mavlink_msg_param_value_pack(system_id, component_id, msg, param_value->param, param_value->type, param_value->value);
}

/**
 * @brief Encode a param_value struct on a channel
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param param_value C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_param_value_encode_chan(uint8_t system_id, uint8_t component_id, uint8_t chan, mavlink_message_t* msg, const mavlink_param_value_t* param_value)
{
    return mavlink_msg_param_value_pack_chan(system_id, component_id, chan, msg, param_value->param, param_value->type, param_value->value);
}

/**
 * @brief Send a param_value message
 * @param chan MAVLink channel to send the message
 *
 * @param param  Идентификатор параметра.
 * @param type  Тип параметра.
 * @param value  Значение параметра.
 */
#ifdef MAVLINK_USE_CONVENIENCE_FUNCTIONS

static inline void mavlink_msg_param_value_send(mavlink_channel_t chan, uint8_t param, uint8_t type, uint64_t value)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_PARAM_VALUE_LEN];
    _mav_put_uint64_t(buf, 0, value);
    _mav_put_uint8_t(buf, 8, param);
    _mav_put_uint8_t(buf, 9, type);

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_PARAM_VALUE, buf, MAVLINK_MSG_ID_PARAM_VALUE_MIN_LEN, MAVLINK_MSG_ID_PARAM_VALUE_LEN, MAVLINK_MSG_ID_PARAM_VALUE_CRC);
#else
    mavlink_param_value_t packet;
    packet.value = value;
    packet.param = param;
    packet.type = type;

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_PARAM_VALUE, (const char *)&packet, MAVLINK_MSG_ID_PARAM_VALUE_MIN_LEN, MAVLINK_MSG_ID_PARAM_VALUE_LEN, MAVLINK_MSG_ID_PARAM_VALUE_CRC);
#endif
}

/**
 * @brief Send a param_value message
 * @param chan MAVLink channel to send the message
 * @param struct The MAVLink struct to serialize
 */
static inline void mavlink_msg_param_value_send_struct(mavlink_channel_t chan, const mavlink_param_value_t* param_value)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    mavlink_msg_param_value_send(chan, param_value->param, param_value->type, param_value->value);
#else
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_PARAM_VALUE, (const char *)param_value, MAVLINK_MSG_ID_PARAM_VALUE_MIN_LEN, MAVLINK_MSG_ID_PARAM_VALUE_LEN, MAVLINK_MSG_ID_PARAM_VALUE_CRC);
#endif
}

#if MAVLINK_MSG_ID_PARAM_VALUE_LEN <= MAVLINK_MAX_PAYLOAD_LEN
/*
  This varient of _send() can be used to save stack space by re-using
  memory from the receive buffer.  The caller provides a
  mavlink_message_t which is the size of a full mavlink message. This
  is usually the receive buffer for the channel, and allows a reply to an
  incoming message with minimum stack space usage.
 */
static inline void mavlink_msg_param_value_send_buf(mavlink_message_t *msgbuf, mavlink_channel_t chan,  uint8_t param, uint8_t type, uint64_t value)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char *buf = (char *)msgbuf;
    _mav_put_uint64_t(buf, 0, value);
    _mav_put_uint8_t(buf, 8, param);
    _mav_put_uint8_t(buf, 9, type);

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_PARAM_VALUE, buf, MAVLINK_MSG_ID_PARAM_VALUE_MIN_LEN, MAVLINK_MSG_ID_PARAM_VALUE_LEN, MAVLINK_MSG_ID_PARAM_VALUE_CRC);
#else
    mavlink_param_value_t *packet = (mavlink_param_value_t *)msgbuf;
    packet->value = value;
    packet->param = param;
    packet->type = type;

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_PARAM_VALUE, (const char *)packet, MAVLINK_MSG_ID_PARAM_VALUE_MIN_LEN, MAVLINK_MSG_ID_PARAM_VALUE_LEN, MAVLINK_MSG_ID_PARAM_VALUE_CRC);
#endif
}
#endif

#endif

// MESSAGE PARAM_VALUE UNPACKING


/**
 * @brief Get field param from param_value message
 *
 * @return  Идентификатор параметра.
 */
static inline uint8_t mavlink_msg_param_value_get_param(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint8_t(msg,  8);
}

/**
 * @brief Get field type from param_value message
 *
 * @return  Тип параметра.
 */
static inline uint8_t mavlink_msg_param_value_get_type(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint8_t(msg,  9);
}

/**
 * @brief Get field value from param_value message
 *
 * @return  Значение параметра.
 */
static inline uint64_t mavlink_msg_param_value_get_value(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint64_t(msg,  0);
}

/**
 * @brief Decode a param_value message into a struct
 *
 * @param msg The message to decode
 * @param param_value C-struct to decode the message contents into
 */
static inline void mavlink_msg_param_value_decode(const mavlink_message_t* msg, mavlink_param_value_t* param_value)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    param_value->value = mavlink_msg_param_value_get_value(msg);
    param_value->param = mavlink_msg_param_value_get_param(msg);
    param_value->type = mavlink_msg_param_value_get_type(msg);
#else
        uint8_t len = msg->len < MAVLINK_MSG_ID_PARAM_VALUE_LEN? msg->len : MAVLINK_MSG_ID_PARAM_VALUE_LEN;
        memset(param_value, 0, MAVLINK_MSG_ID_PARAM_VALUE_LEN);
    memcpy(param_value, _MAV_PAYLOAD(msg), len);
#endif
}
